﻿#include <iostream>
#include <cmath>

double f(double x) {
    return 1.0 / (1.0 + x);
}

double leftRectangle(double a, double b, int n) {
    double h = (b - a) / n;
    double integral = 0.0;
    for (int i = 0; i < n; i++) {
        double x = a + i * h;
        integral += f(x) * h;
    }
    return integral;
}

double rightRectangle(double a, double b, int n) {
    double h = (b - a) / n;
    double integral = 0.0;
    for (int i = 0; i < n; i++) {
        double x = a + (i + 1) * h;
        integral += f(x) * h;
    }
    return integral;
}

int main() {
    double a, b, eps;
    int n;

    std::cout << "Enter the start of the integration interval a: ";
    std::cin >> a;
    std::cout << "Enter the end of the integration interval b: ";
    std::cin >> b;
    std::cout << "Enter the required computation accuracy epsilon: ";
    std::cin >> eps;
    std::cout << "Enter the initial number of partitions n: "; // число разбиений
    std::cin >> n;

    while (true) {
        double I_left = leftRectangle(a, b, n);
        double I_left_2n = leftRectangle(a, b, 2 * n);
        double I_right = rightRectangle(a, b, n);
        double I_right_2n = rightRectangle(a, b, 2 * n);

        if (std::abs(I_left - I_left_2n) < eps && std::abs(I_right - I_right_2n) < eps) {
            std::cout << "Approximate value of the integral by the left rectangle method: "  << I_left << std::endl;
            std::cout << "Approximate value of the integral by the right rectangle method: "  << I_right << std::endl;
            break;
        }

        n *= 2;
    }

    return 0;
}